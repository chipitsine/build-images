#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

echo "ref: $CI_COMMIT_REF_NAME"
echo "namespace: $CI_PROJECT_NAMESPACE"
echo "project: $CI_PROJECT_NAME"

master_build=0
if test "$CI_COMMIT_REF_NAME" = "master" && test "$CI_PROJECT_NAMESPACE" = "openconnect" && test "$CI_PROJECT_NAME" = "build-images";then
	master_build=1
fi

set -e
echo " * Logging in to $registry"
podman login -u gitlab-ci-token -p $CI_JOB_TOKEN $registry

echo " * Building"
podman build -t $registry/$project:$image $startdir
master_build=1

if test $master_build = 0;then
	echo "Not a master build"
	exit 0
else
	echo " * Pushing to $registry"
	podman push $registry/$project:$image
fi

echo " * Logout"
podman logout $registry

exit 0
